# Spring Cloud Sample Client #

The application obtains its configuration from the spring cloud configuration server (localhost:8001), registers with Eureka registry.

Configuration updates can be pushed by cloud bus.
<pre>curl -X POST  http://localhost:8001/bus/refresh</pre>